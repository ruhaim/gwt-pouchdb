package it.netgrid.gwt.pouchdb.options;

import it.netgrid.gwt.pouchdb.ArrayUtils;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public class GetDocOptions extends JavaScriptObject {
	protected GetDocOptions() {}
	public final native void setRev(String rev) /*-{
		this.rev = rev;
	}-*/;
	
	public final native String getRev() /*-{
		return this.rev;
	}-*/;

	public final native void setRevs(boolean revs) /*-{
		this.revs = revs;
	}-*/;
	
	public final native boolean isRevs() /*-{
		return this.revs == true;
	}-*/;

	public final native void setConflicts(boolean conflicts) /*-{
		this.conflicts = conflicts;
	}-*/;
	
	public final native boolean isConflicts() /*-{
		return this.conflicts == true;
	}-*/;

	public final native void setAttachments(boolean attachments) /*-{
		this.attachments = attachments;
	}-*/;
	
	public final native boolean isAttachments() /*-{
		return this.attachments == true;
	}-*/;

	public final native void setLocalSeq(boolean localSeq) /*-{
		this.local_seq = localSeq;
	}-*/;
	
	public final native boolean isLocalSeq() /*-{
		return this.local_seq == true;
	}-*/;
	
	public final void setOpenRevs(List<String> revs) {
		this.setOpenRevs(ArrayUtils.toJsArrayString(revs));
	}

	public final native void setOpenRevs(JsArrayString revs) /*-{
		this.open_revs = revs;
	}-*/;
	
	public final native JsArrayString getOpenRevs() /*-{
		return this.open_revs;
	}-*/;

	public final native void setRevsInfo(boolean revsInfo) /*-{
		this.revs_info = revsInfo;
	}-*/;
	
	public final native boolean isRevsInfo() /*-{
		return this.revs_info == true;
	}-*/;
}
