package it.netgrid.gwt.pouchdb.options;

import com.google.gwt.core.client.JavaScriptObject;

public class CompactionOptions extends JavaScriptObject {
	
	protected CompactionOptions() {}
	
	public final native int getInterval() /*-{
		return this.interval;
	}-*/;
	
	public final native void setInterval(int millis) /*-{
		this.interval = millis;
	}-*/;
}