package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.ReplicationResult;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;

public interface IReplicationCompleteHandler {

	public void onError(ErrorResponse err);

	public void onSuccess(ReplicationResult result);
}
