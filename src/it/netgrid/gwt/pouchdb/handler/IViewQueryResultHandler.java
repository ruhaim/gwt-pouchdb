package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.ErrorResponse;
import it.netgrid.gwt.pouchdb.response.QueryResponse;

import com.google.gwt.core.client.JavaScriptObject;

public interface IViewQueryResultHandler<T extends JavaScriptObject> {
	public void onError(ErrorResponse error);

	public void onSuccess(QueryResponse<T> response);
}
