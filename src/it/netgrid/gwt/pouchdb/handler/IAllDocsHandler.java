package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.AllDocsResponse;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;


public interface IAllDocsHandler {

	public void onSuccess(AllDocsResponse response);

	public void onError(ErrorResponse error);

}
