package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.DocChangeEvent;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;

import com.google.gwt.core.client.JsArray;

public interface IChangeCompleteHandler {

	public void onSuccess(JsArray<DocChangeEvent> changes);

	public void onError(ErrorResponse error);

}
