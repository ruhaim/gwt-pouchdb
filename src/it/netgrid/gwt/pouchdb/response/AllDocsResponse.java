package it.netgrid.gwt.pouchdb.response;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class AllDocsResponse extends JavaScriptObject {
	protected AllDocsResponse() {
	}

	public final native int getTotalRows() /*-{
		return this.total_rows;
	}-*/;

	public final native JsArray<DocChangeEvent> getRows() /*-{
		return this.rows;
	}-*/;
}
