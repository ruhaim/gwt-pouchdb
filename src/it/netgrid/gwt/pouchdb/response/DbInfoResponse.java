package it.netgrid.gwt.pouchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public class DbInfoResponse extends JavaScriptObject {

	protected DbInfoResponse() {
	}

	public final native String getDbName() /*-{
		return this.db_name;
	}-*/;

	public final native void setDbName(String dbName) /*-{
		this.db_name = dbName;
	}-*/;

	public final native int getDocCount() /*-{
		return this.doc_count;
	}-*/;

	public final native void setDocCount(int docCount) /*-{
		this.doc_count = docCount;
	}-*/;

	public final native int getUpdateSeq() /*-{
		return this.update_seq;
	}-*/;

	public final native void setUpdateSeq(int updateSeq) /*-{
		this.update_seq = updateSeq;
	}-*/;
}
