package it.netgrid.gwt.pouchdb;

import it.netgrid.gwt.pouchdb.handler.IAllDbsHandler;
import it.netgrid.gwt.pouchdb.handler.IAllDocsHandler;
import it.netgrid.gwt.pouchdb.handler.ICompactionHandler;
import it.netgrid.gwt.pouchdb.handler.IDbInfoHandler;
import it.netgrid.gwt.pouchdb.handler.IDocGetHandler;
import it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler;
import it.netgrid.gwt.pouchdb.handler.IGenericHandler;
import it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler;
import it.netgrid.gwt.pouchdb.handler.IQueryResultHandler;
import it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler;
import it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler;
import it.netgrid.gwt.pouchdb.options.AllDocsOptions;
import it.netgrid.gwt.pouchdb.options.ChangesOptions;
import it.netgrid.gwt.pouchdb.options.CompactionOptions;
import it.netgrid.gwt.pouchdb.options.DbConnectionOptions;
import it.netgrid.gwt.pouchdb.options.GetDocOptions;
import it.netgrid.gwt.pouchdb.options.ReplicationOptions;
import it.netgrid.gwt.pouchdb.options.ViewQueryOptions;
import it.netgrid.gwt.pouchdb.response.QueryResponse;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class PouchDb implements IPouchDb {

	public final native static void allDbs(IAllDbsHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDbsHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDbsHandler::onSuccess(Lcom/google/gwt/core/client/JsArrayString;)(data);
		}
		$wnd.PouchDB.allDbs(callbackFunc);
	}-*/;

	public static final native void destroy(String name) /*-{
		$wnd.PouchDB.destroy(name);
	}-*/;

	public static final native void destroy(String name,
			IGenericHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IGenericHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IGenericHandler::onSuccess(Lcom/google/gwt/core/client/JavaScriptObject;)(data);
		}
		$wnd.PouchDB.destroy(name, callbackFunc);
	}-*/;

	public final native static boolean getEnableAllDbs() /*-{
		return $wnd.PouchDB.enableAllDbs;
	}-*/;

	public final native static void replicate(String source, String target) /*-{
		$wnd.PouchDB.replicate(source, target);
	}-*/;

	public final native static void replicate(String source, String target,
			ReplicationOptions options) /*-{
		$wnd.PouchDB.replicate(source, target, options);
	}-*/;

	public final native static void setEnableAllDbs(boolean value) /*-{
		$wnd.PouchDB.enableAllDbs = value;
	}-*/;

	private JavaScriptObject instance;

	public PouchDb(String name) {
		this.init(name);
	}

	public PouchDb(String name, DbConnectionOptions options) {
		this.init(name, options);
	}

	@Override
	public final native void allDocs(IAllDocsHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDocsHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDocsHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/AllDocsResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance
				.allDocs({}, callbackFunc);
	}-*/;

	@Override
	public final native void allDocs(IAllDocsHandler callback,
			AllDocsOptions options) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDocsHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IAllDocsHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/AllDocsResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.allDocs(options,	callbackFunc);
	}-*/;

	@Override
	public final native <D extends PouchDbDoc> void bulkDocs(JsArray<D> docs) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.bulkDocs({"docs":docs});
	}-*/;

	@Override
	public final native <D extends PouchDbDoc> void bulkDocs(JsArray<D> docs, IDocUpdateHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/DocUpdateResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.bulkDocs({"docs":docs}, callbackFunc);
	}-*/;

	@Override
	public final <D extends PouchDbDoc> void  bulkDocs(List<D> docs) {
		this.bulkDocs(ArrayUtils.toJsArrayDocs(docs));
	}

	@Override
	public final <D extends PouchDbDoc> void bulkDocs(List<D> docs, IDocUpdateHandler callback) {
		this.bulkDocs(ArrayUtils.toJsArrayDocs(docs), callback);
	}

	@Override
	public final native ChangesFeed changes(ChangesOptions options) /*-{
		return this.@it.netgrid.gwt.pouchdb.PouchDb::instance.changes(options);
	}-*/;

	@Override
	public final native void compact() /*-{
		var callbackFunc = function() {
			callback.@it.netgrid.gwt.pouchdb.handler.ICompactionHandler::onCompactionDone()();
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.compact();
	}-*/;

	@Override
	public final native void compact(ICompactionHandler callback) /*-{
		var callbackFunc = function() {
			callback.@it.netgrid.gwt.pouchdb.handler.ICompactionHandler::onCompactionDone()();
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.compact({}, callback);
	}-*/;

	@Override
	public final native void compact(ICompactionHandler callback,
			CompactionOptions options) /*-{
		var callbackFunc = function() {
			callback.@it.netgrid.gwt.pouchdb.handler.ICompactionHandler::onCompactionDone()();
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.compact(options,	callback);
	}-*/;

	@Override
	public final native void get(String id, IDocGetHandler<?> callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocGetHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocGetHandler::onSuccess(Lit/netgrid/gwt/pouchdb/PouchDbDoc;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.get(id, callbackFunc);
	}-*/;

	@Override
	public final native void get(String id, IDocGetHandler<?> callback,
			GetDocOptions options) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocGetHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocGetHandler::onSuccess(Lit/netgrid/gwt/pouchdb/PouchDbDoc;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.get(id, options,	callbackFunc);
	}-*/;

	@Override
	public final native void info(IDbInfoHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDbInfoHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDbInfoHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/DbInfoResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.info(callbackFunc);
	}-*/;

	private final native void init(String name) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance = new $wnd.PouchDB(name);
	}-*/;

	private final native void init(String name, DbConnectionOptions options) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance = new $wnd.PouchDB(name,
				options);
	}-*/;

	@Override
	public final native void post(PouchDbDoc doc) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.post(doc);
	}-*/;

	@Override
	public final native void post(PouchDbDoc doc, IDocUpdateHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/DocUpdateResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.post(doc, callbackFunc);
	}-*/;

	@Override
	public final native void put(PouchDbDoc doc) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.put(doc);
	}-*/;

	@Override
	public final native void put(PouchDbDoc doc, IDocUpdateHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/DocUpdateResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.put(doc, callbackFunc);
	}-*/;

	@Override
	public void query(AQueryMixed query, IQueryMixedResultHandler callback) {
		switch (query.getReductionType()) {
		case SUM:
			this.queryDefaultReduction("_sum", query, callback);
			break;
		case STATS:
			this.queryDefaultReduction("_stats", query, callback);
			break;
		case COUNT:
			this.queryDefaultReduction("_count", query, callback);
			break;
		case CUSTOM:
			this.queryCustomReduction(query, callback);
			break;
		case NONE:
		default:
			this.queryWithoutReduction(query, callback);
			break;
		}
	}

	private final native void queryCustomReduction(AQueryMixed query,
			IQueryMixedResultHandler callback) /*-{

		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;) (err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({map: query.@it.netgrid.gwt.pouchdb.AQueryMixed::map(Lit/netgrid/gwt/pouchdb/PouchDbDoc;)}, {reduce: query.@it.netgrid.gwt.pouchdb.AQueryMixed::reduce(Lcom/google/gwt/core/client/JsArrayMixed;Lcom/google/gwt/core/client/JsArrayMixed;Z)}, callbackFunc);
	}-*/;

	private final native void queryDefaultReduction(String reduction,
			AQueryMixed query, IQueryMixedResultHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;) (err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({map: query.@it.netgrid.gwt.pouchdb.AQueryMixed::map(Lit/netgrid/gwt/pouchdb/PouchDbDoc;)}, {reduce: reduction}, callbackFunc);
	}-*/;

	private final native void queryWithoutReduction(AQueryMixed query,
			IQueryMixedResultHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({
			map : query.@it.netgrid.gwt.pouchdb.AQueryMixed::map(Lit/netgrid/gwt/pouchdb/PouchDbDoc;)
		}, {
			reduce : false
		}, callbackFunc);
	}-*/;

	@Override
	public <D extends JavaScriptObject> void query(AQuery<D,?,?> query, IQueryResultHandler<D> callback) {
		switch (query.getReductionType()) {
		case SUM:
			this.queryDefaultReduction("_sum", query, callback);
			break;
		case STATS:
			this.queryDefaultReduction("_stats", query, callback);
			break;
		case COUNT:
			this.queryDefaultReduction("_count", query, callback);
			break;
		case CUSTOM:
			this.queryCustomReduction(query, callback);
			break;
		case NONE:
		default:
			this.queryWithoutReduction(query, callback);
			break;
		}
	}
	
//	@Override 
//	public void queryView(String view, ViewQueryOptions options, IViewQueryResultHandler<?> callback) {
//		String opt = options.getUrlEncodedString();
//		String query = opt == null ? view : view + "?" + opt;
//		this.queryView(query, callback);
//	}

	@Override 
	public final native void queryView(String view, IViewQueryResultHandler<?> callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler::onSuccess(*)(data)
		};
		
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query(view,callbackFunc);
	}-*/;

	@Override 
	public final native void queryView(String view, ViewQueryOptions options, IViewQueryResultHandler<?> callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler::onSuccess(*)(data)
		};
		
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query(view, options, callbackFunc);
	}-*/;

	@Override 
	public final native void queryView(String view, IViewQueryMixedResultHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler::onSuccess(*)(data)
		};
		
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query(view,callbackFunc);
	}-*/;

	@Override 
	public final native void queryView(String view, ViewQueryOptions options, IViewQueryMixedResultHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler::onSuccess(*)(data)
		};
		
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query(view, options, callbackFunc);
	}-*/;

	private final native void queryCustomReduction(AQuery<?,?,?> query, IQueryResultHandler<?> callback) /*-{

		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({map: query.@it.netgrid.gwt.pouchdb.AQueryMixed::map(*)}, {reduce: query.@it.netgrid.gwt.pouchdb.AQueryMixed::reduce(*)}, callbackFunc);
	}-*/;

	private final native void queryDefaultReduction(String reduction,
			AQuery<?,?,?> query, IQueryResultHandler<?> callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onError(*) (err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({map: query.@it.netgrid.gwt.pouchdb.AQuery::map(*)}, {reduce: reduction}, callbackFunc);
	}-*/;

	private final native void queryWithoutReduction(AQuery<?,?,?> query, IQueryResultHandler<?> callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onError(*)(err)
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IQueryResultHandler::onSuccess(*)(data)
		};

		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.query({
			map : query.@it.netgrid.gwt.pouchdb.AQuery::map(*)
		}, {
			reduce : false
		}, callbackFunc);
	}-*/;

	@Override
	public final native void remove(PouchDbDoc doc) /*-{
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.remove(doc);
	}-*/;

	@Override
	public final native void remove(PouchDbDoc doc, IDocUpdateHandler callback) /*-{
		var callbackFunc = function(err, data) {
			if (err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler::onSuccess(Lit/netgrid/gwt/pouchdb/response/DocUpdateResponse;)(data);
		}
		this.@it.netgrid.gwt.pouchdb.PouchDb::instance.remove(doc, callbackFunc);
	}-*/;
	
	public static final <D extends JavaScriptObject> List<D> toList(JsArray<D> array) {
		ArrayList<D> retval = new ArrayList<D>();
		
		if(array == null) return retval;
		
		for(int i = 0; i<array.length(); i++) {
			retval.add(array.get(i));
		}
		
		return retval;
	}

	@Override
	public <D extends JavaScriptObject> List<D> responseToList(QueryResponse<D> response) {
		return PouchDb.toList(response);
	}
	
	public static final <D extends JavaScriptObject> List<D> toList(QueryResponse<D> response) {
		ArrayList<D> retval = new ArrayList<D>();
		
		if(response == null) return retval;
		
		for(int i = 0; i<response.getRows().length(); i++) {
			retval.add(response.getRows().get(i).getValue());
		}
		
		return retval;		
	}

	@Override
	public <D extends JavaScriptObject> List<D> arrayToList(JsArray<D> array) {
		return PouchDb.toList(array);
	}
}
